package com.example.esther.healthix;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by esther on 7/28/16.
 */
public class GridCustomAdapter extends BaseAdapter {
    Context context;
    String doctorList[];
    int flags[];
    LayoutInflater inflter;

    public GridCustomAdapter(Context applicationContext, String[] countryList, int[] flags) {
        this.context = context;
        this.doctorList = countryList;
        this.flags = flags;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return doctorList.length;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.doc_page_list, null);
        TextView country = (TextView) view.findViewById(R.id.txtViewDescription);
        ImageView icon = (ImageView) view.findViewById(R.id.imgViewLogo);
        country.setText(doctorList[i]);
        icon.setImageResource(flags[i]);
        return view;
    }
}



