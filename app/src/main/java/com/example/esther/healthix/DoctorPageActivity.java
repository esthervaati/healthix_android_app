package com.example.esther.healthix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class DoctorPageActivity extends AppCompatActivity {
    ListView simpleList;
    String doctorList[] = {"Find Patient", "Send eRX", "Change Prescription", "Recent Prescription", "Regenerate Code", "Refills"};
    int flags[] = {R.drawable.doctors, R.drawable.prescription, R.drawable.ambulance, R.drawable.hospital, R.drawable.doctors, R.drawable.hospital};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacies_page);
        simpleList = (ListView) findViewById(R.id.simpleListView);
        GridCustomAdapter  customAdapter = new GridCustomAdapter(getApplicationContext(), doctorList, flags);
        simpleList.setAdapter(customAdapter);
        simpleList.setBackgroundResource(R.drawable.border);
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                long viewId = view.getId();
                if (position ==0) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,FindPatientActivity.class);
                    startActivity(intentfind);
                } else if (position ==1) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,SendErxActivity.class);
                    startActivity(intentfind);
                   // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==2) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,ChangePrescriptionDocActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==3) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,RecentPrescriptionsActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==4) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,DocRegenerateActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==5) {
                    Intent intentfind = new Intent(DoctorPageActivity.this,REfillDocActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }


            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_messages:
                Toast.makeText(this, "Refresh selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            // action with ID action_settings was selected
            case R.id.action_reports:
                Toast.makeText(this, "Settings selected", Toast.LENGTH_SHORT)
                        .show();
                break;
            default:
                break;
        }

        return true;
    }



}

