package com.example.esther.healthix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    Button btn_signIn;
    EditText email, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
    }

    public void signIn(View view) {
        //btn_signIn =(Button)findViewById(R.id.email_sign_in_button)
        if (email.getText().toString().equals("michel@yahoo.com") &&
                password.getText().toString().equals("123")) {
            Toast.makeText(getApplicationContext(), "Login successful",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, DoctorPageActivity.class);
            startActivity(intent);


        } else if (email.getText().toString().equals("j@gmai.co") &&
                password.getText().toString().equals("1234")) {
            Toast.makeText(getApplicationContext(), "Login successful",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(LoginActivity.this, PharmaciesPageActivity.class);
            startActivity(intent);
        }

            else{
                Toast.makeText(getApplicationContext(), "Username and password is NOT correct",
                        Toast.LENGTH_SHORT).show();

            }
        }
    }


