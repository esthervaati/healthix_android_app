package com.example.esther.healthix;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        Typeface iconFont = FontManager.getTypeface(getApplicationContext(), FontManager.FONTAWESOME);
        FontManager.markAsIconContainer(findViewById(R.id.icons_container), iconFont);
//        TextView tx1= (TextView) findViewById(R.id.tv1);
//        TextView tx2 = (TextView)findViewById(R.id.tv2);
//        TextView tx3= (TextView)findViewById(R.id.tv3);
//
//        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/iconmoon.ttf");
//
//        tx1.setTypeface(custom_font);
//        tx2.setTypeface(custom_font);
//        tx3.setTypeface(custom_font);



    }
}
