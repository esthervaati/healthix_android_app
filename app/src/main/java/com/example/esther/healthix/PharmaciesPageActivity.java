package com.example.esther.healthix;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

public class PharmaciesPageActivity extends AppCompatActivity {
    ListView simpleList;
    String countryList[] = {"Find Patient","Send eRX", "Change Prescription", "Recent Prescription","Regenerate Code","Refills" };
    int flags[] = {R.drawable.doctors, R.drawable.prescription, R.drawable.ambulance, R.drawable.hospital,R.drawable.ambulance, R.drawable.hospital};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pharmacies_page);
        simpleList = (ListView) findViewById(R.id.simpleListView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), countryList, flags);
        simpleList.setAdapter(customAdapter);
        simpleList.setBackgroundResource(R.drawable.border);
        simpleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                long viewId = view.getId();
                if (position ==0) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,FindPatientActivity.class);
                    startActivity(intentfind);
                } else if (position ==1) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,SendErxActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==2) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,ChangePrescriptionDocActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==3) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,ResultsActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==4) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,ChangePrescriptionDocActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }
                else if (position ==5) {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,ChangePrescriptionDocActivity.class);
                    startActivity(intentfind);
                    // Toast.makeText(this, "Button 2 clicked", Toast.LENGTH_SHORT).show();
                }

                else {
                    Intent intentfind = new Intent(PharmaciesPageActivity.this,FindPatientActivity.class);
                    startActivity(intentfind);
                    //Toast.makeText(this, "ListView clicked" + position, Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}