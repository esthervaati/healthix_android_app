package com.example.esther.healthix;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class ResultsActivity extends AppCompatActivity {

    ArrayList<String> StringArray = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);

        ArrayValueAddFunction();

        LinearLayout LinearLayoutView = new LinearLayout(this);
        TextView DisplayStringArray = new TextView(this);
        DisplayStringArray.setTextSize(15);
        LinearLayoutView.addView(DisplayStringArray);
        for (int i=0; i<StringArray.size();i++){
            DisplayStringArray.append(StringArray.get(i));
            DisplayStringArray.append("\n");
        }
        setContentView(LinearLayoutView);
    }

    private void ArrayValueAddFunction() {
        StringArray.add("Doctor Number : DKT1000");
        StringArray.add("Diagnosis :null");
        StringArray.add("Drug Name : GILDESS FE 1.5/30 28 DAY (Pack)");
        StringArray.add("Dosage: Mixed");
        StringArray.add("Formulation: Pack");
        StringArray.add("route_of_administration: Pack");
        StringArray.add("rate_of_administration: 3 ");
        StringArray.add("duration_of_treatment: 1 week");
        StringArray.add("frequency_of_administration: 3 hours");
        StringArray.add("substitutable: true");
        StringArray.add("dangerous_drug: true");
        StringArray.add("hospital_name: The Karen Hospital");
        StringArray.add("patient_type: Outpatient");
        StringArray.add("refills: null");
        StringArray.add("start_date: 2016-07-05");

    }
}