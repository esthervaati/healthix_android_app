package com.example.esther.healthix;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Locale;

public class GeneralActivity extends AppCompatActivity {
    /**
     * Called when the activity is first created.
     */

    private GridviewAdapter mAdapter;
    private ArrayList<String> listProviders;
    private ArrayList<Integer> listIcons;

    private GridView gridView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);


        prepareList();

        // prepared arraylist and passed it to the Adapter class
        mAdapter = new GridviewAdapter(this, listProviders, listIcons);

        // Set custom adapter to gridview
        gridView = (GridView) findViewById(R.id.gridView1);
        gridView.setAdapter(mAdapter);

        // Implement On Item click listener
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position,
                                    long arg3) {


                if (position ==0) {
                    Intent intentP = new Intent(GeneralActivity.this, FindDoctorsActivity.class);
                    startActivity(intentP);

                }
                if (position ==1) {
                    String uri = String.format(Locale.ENGLISH, "geo:%f,%f", -1.296293, 36.802167);
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                    startActivity(intent);

                }
                if (position ==2) {
                    Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_SHORT).show();
//                    Intent intentP = new Intent(GeneralActivity.this, FindDoctorsActivity.class);
//                    startActivity(intentP);

                }
                if (position ==3) {
                    Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_SHORT).show();

                }
                if (position ==4) {
                    Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_SHORT).show();

                }
                if (position ==5) {
                    Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_SHORT).show();

                }
                if (position ==6) {
                    Toast.makeText(getApplicationContext(),"No Records Found",Toast.LENGTH_SHORT).show();

                }
                if (position ==7) {
                    Intent intentP = new Intent(GeneralActivity.this, LoginActivity.class);
                    startActivity(intentP);

                }
            }


        });

}
    public void prepareList() {
        listProviders = new ArrayList<String>();

        listProviders.add("Doctors");
        listProviders.add("Pharmacists");
        listProviders.add("Ambulances");
        listProviders.add("Hospitals");
        listProviders.add("Clinics");
        listProviders.add("Laboratories");
        listProviders.add("News");
        listProviders.add("My Account ");


        listIcons = new ArrayList<Integer>();
        listIcons.add(R.drawable.doc);
        listIcons.add(R.drawable.prescr);
        listIcons.add(R.drawable.ambula);
        listIcons.add(R.drawable.hospital);
        listIcons.add(R.drawable.fill);
        listIcons.add(R.drawable.flask);
        listIcons.add(R.drawable.news);
        listIcons.add(R.drawable.login);

    }
}
